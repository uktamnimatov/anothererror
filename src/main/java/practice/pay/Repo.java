package practice.pay;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class Repo {

    JdbcTemplate jdbcTemplate;

    @Autowired
    public Repo setJdbcTemplate(DataSource postgres, JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = new JdbcTemplate(postgres);
        return this;
    }

    public void getDataFromTable() {
        String sql = "select * from user_info";
        jdbcTemplate.query(sql, new RowMapper<Object>() {
            @Override
            public Object mapRow(ResultSet resultSet, int i) throws SQLException {
                System.out.println(resultSet.getString("name"));
                System.out.println(resultSet.getString("password"));
                System.out.println(resultSet.getString("email"));
                System.out.println(resultSet.getString("username"));
                return null
                        ;
            }
        });
    }

    public List<UserData2> getDataFromTableInList() {
        String sql = "select * from user_info";
        List<UserData2> list = new ArrayList<>();
        jdbcTemplate.query(sql, new RowMapper<Object>() {
            @Override
            public Object mapRow(ResultSet resultSet, int i) throws SQLException {
                list.add(new UserData2().setEmail(resultSet.getString("email"))
                        .setName(resultSet.getString("name"))
                        .setPassword(resultSet.getString("password"))
                        .setUsername(resultSet.getString("username")));
                return null;
            }
        });
        return list;
    }

    public void insertIntoTable(UserData2 userData2) {
        String sql = "insert into user_info(name, password, username, email) values("
                + userData2.getName() + ", "
                + userData2.getPassword() + " , "
                + userData2.getUsername() + ","
                + userData2.getEmail() + ")";
        jdbcTemplate.execute(sql);
    }
}
