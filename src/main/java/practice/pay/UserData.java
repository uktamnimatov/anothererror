package practice.pay;

public class UserData {

    private String userName;
    private String userPassword;
    private String userEmail;
    private int userID;

    public int getUserID() {
        return userID;
    }

    public UserData setUserID(int userID) {
        this.userID = userID;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public UserData setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public UserData setUserPassword(String userPassword) {
        this.userPassword = userPassword;
        return this;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public UserData setUserEmail(String userEmail) {
        this.userEmail = userEmail;
        return this;
    }
}
