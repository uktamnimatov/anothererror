package practice.pay;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ControllerClass {

    List<Object> listOfUsers = new ArrayList<>();

    Repo repo;

    @Autowired
    public ControllerClass setRepo(Repo repo) {
        this.repo = repo;
        return this;
    }

    @GetMapping(value = "/")
    public String indexPage(){
        repo.insertIntoTable(new UserData2().setEmail("uktam").setName("Uktamjon")
        .setPassword("uktam@1997").setUsername("uktamjon"));
        return "login";
    }

    @GetMapping(value = "/login.html")
    public String showLoginPage(){
        return "login";
    }

    @GetMapping(value = "/charts.html")
    public String showCharts(){
        return "charts";
    }

    @GetMapping(value = "/forgot-password.html")
    public String showForgotPasswordPage(){
        return "forgot-password";
    }

    @PostMapping(value = "/doRegistration")
    public String doRegistration(Model model, UserData userData){
        model.addAttribute(userData);
        listOfUsers.add(userData);
        userData.setUserID(listOfUsers.size());
        System.out.println(userData.getUserName());
        System.out.println(userData.getUserPassword());
        System.out.println(userData.getUserEmail());
        System.out.println(userData.getUserID());
        setRepo(repo);
        model.addAttribute(repo);
        return "index";
    }

    @GetMapping(value = "/forms.html")
    public String showForms(){
        return "forms";
    }

    @GetMapping(value = "/index.html")
    public String showDashboard(){
        return "index";
    }

    @GetMapping(value = "/maps.html")
    public String showMaps(){
        return "maps";
    }

    @GetMapping(value = "/sign-up.html")
    public String showSignUpPage(){
        return "sign-up";
    }

    @GetMapping(value = "/ui-buttons.html")
    public String showUiButtons(){
        return "ui-buttons";
    }

    @GetMapping(value = "/ui-cards.html")
    public String showUiCards(){
        return "ui-cards";
    }

    @GetMapping(value = "/ui-colors.html")
    public String showUiColors(){
        return "ui-colors";
    }

    @GetMapping(value = "/ui-components.html")
    public String showUiComponents(){
        return "ui-components";
    }

    @GetMapping(value = "/ui-form-components.html")
    public String showUiFormComponents(){
        return "ui-form-components";
    }

    @GetMapping(value = "/ui-icons.html")
    public String showUiIcons(){
        return "ui-icons";
    }

    @GetMapping(value = "/ui-list-components.html")
    public String showUiListComponents(){
        return "ui-list-components";
    }

    @GetMapping(value = "/ui-tables.html")
    public String showUiTables(){
        return "ui-tables";
    }

    @GetMapping(value = "/ui-typography.html")
    public String showUiTypography(){
        return "ui-typography";
    }
}
